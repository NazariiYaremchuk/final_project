<%--
  Created by IntelliJ IDEA.
  User: Nazarij
  Date: 04.10.2020
  Time: 18:57
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Login Page</title>
</head>
<body>
<h1>Вхід в систему</h1><br/>
<form method="get" action="${pageContext.request.contextPath}/app/login">

    <input type="text" name="name"><br/>
    <input type="password" name="pass"><br/><br/>
    <input class="button" type="submit" value="Войти">

</form>
<br/>
<a href="${pageContext.request.contextPath}/app/logout">На головну</a>

</body>
</html>
